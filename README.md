
# Vity Tables

Tablas dinámicas como plugin para usar como datatables en vue

## Instalación:

  

* Se importa VityTables como un componente más. Primero con

~~~
import VityTable from 'vitytables'
~~~
* Se agrega como componente

~~~
components: {
	VityTable
}
~~~

* Y se inicializa

~~~
<VityTable />
~~~

## Parametros:

Los campos con un * son opcionales.

*  **columns**: Requerida. Son las columnas de la tabla. Se mandaran como un array contenedor de objetos que cada objeto con los siguientes campos:

	*  **name** -> (String) Nombre de columna visible en la tabla

	*  **dbName** -> (String) El nombre de la columna a la que corresponde en el back end. Si se incluye un "." en este campo se entiende que en la devolucion habra un objeto y se debera usar una propiedad de el, aunque este tipo de llamadas deberan ser marcadas como no buscables ni ordenables. Almenos hasta que se encuentre una mejora

	*  **orderable** -> (Boolean) Si es posible ordenar la tabla por esa columna o no

  	*  **searchable*** -> (Boolean) Opcional. Cuando se realice la busqueda, si ese campo es buscable o no. Defecto true.

	*  **date***: (Boolean) Opcional. Si es una fecha para poder formatearla, aunque recuerda que se usara Date para formatear y la fecha pasada debe poder verse con Date de js. Si no se incluye formato (siguiente parametro) se dara por defecto d/m/Y H:i:s

	*  **dateFormat***: (String) Opcional. Es el formato que aplicara la fecha anteriormente dada, sigue la siguiente nomenclatura, aunque hay que tener en cuenta que tanto texto extra como barras de separacion son a eleccion del usuario. Depende de como sea el feedback, seguramente lo cambie a % y simbolo a buscar para fecha pero ya iremos viendo. Ejemplos ('d/m/Y H:i:s', 'Hoy es d de m del Y a las H@i.s'):
		* **d**: Dia del mes con 0 delante.
		* **j**: Dia del mes sin 0 delante.
		* **n**: Numero del mes sin 0 delante.
		* **m**: Numero del mes con 0 delante.
		* **Y**: Numero del año en formato 4 digitos.
		* **H**: Hora en formato 24 horas con 0 delante.
		* **h**: Hora en formato 12 horas con 0 delante.
		* **i**: Minutos con 0 delante. (Me baso en el date de php que no incluye sin 0 delante, ya creare algo)
		* **s**: Segundos con 0 delante. (Me baso en el date de php que no incluye sin 0 delante, ya creare algo)

	*  **template***: (String) Opcional. Es un template en html con el codigo a insertar en la columna siendo dinamico por fila insertando algun dato de alguna columna. Este dato se conseguira mediante el string 'TPLData' el cual se sustituira por el valor de la columna que se declare en reference. Ejemplo (`<a href="/prueba/TPLData">EDITAR</a>`)

	* **reference*** (String) Opcional, aunque obligatorio si se incluye template. Inserta el valor de la columna aqui indicada en el texto que en template se ha definido. Debe ser un nombre de columna correspondiente con los pasados por parametro anteriormente.

*  **url**: Requerida. Se mandara como un String que indicara la url de peticiones dinamica de datos para la tabla. Si cambia duarante la ejecucion vuelve a cargar los datos.

*  **orderBy***: Opcional. Por defecto se ordenara la primera columna en formato Descendente. Se enviara como un objeto que contendra:

	*  **column** -> (Int) La columna a ordenar como un numero de la posicion del array de columns.

	*  **type** -> El tipo de orden que seguirá, tendrá formato 0 si es Descendente y 1 si es Ascendente.

*  **paginate***: Opcional. Recibe un numero que indica el numero de visualizaciones por pagina que se realizara. Por ejemplo, por defecto 10 por pagina. Por defecto sera 10

*  **perPage***: Opcional. Se recibira un array de numeros que seran las cantidades por pagina que se veran. Por defecto sera [5, 10, 25, 50, 100]

*  **params***: Opcional. Parametros extra que son enviados junto a los parametros de llamada dinamica. Por ejemplo el token de acceso o alguna llamada especifica. se incluye un objeto javascript. Por defecto esta vacio. Si cambia duarante la ejecucion vuelve a cargar los datos.

* **swipeResult***: Opcional. Si se rellena cambiara el valor de las columnas que se digan, si coinciden con alguno de los resultados dados, por el valor declarado. Asi por ejemplo se cambiaria un 0 o  false por un NO y un 1 o true por un SI. Por ejemplo. Tiene la estructura de objeto e incluye estos campos.
	* **column**: (Array) La posicion de la o las columnas a aplicar dentro del array de columnas.
	* **data**: (Objecto) El valor a comprobar como key de la fila, y el valor por el que cambiar en el valor de la fila. Ejemplo: '0': 'NO'

## Llamada Dinamica:

La llamada dinamica se hara con axios. Incluira antes que nada los parametros opcionales anteriormente nombrados. El nombre de los campos a llamar es dinamico pudiendo configurarlo desde parametros de entrada. Si no se configuran tendran el formato por defecto dado por mi.

*  **paramsSendOrderColumn** -> El dbName de la columna que esta activada para ordenarse por ella. Por defecto es **orderBy[column]**

*  **paramsSendOrderOrder** -> El tipo de orden que se realizara en dicha coluna. Que valor tiene ascendente y descendente se vera lo siguiente. Por defecto es **orderBy[order]**

*  **paramsSendOrderOrderDesc** -> Valor Descendente para enviar a la peticion. Por defecto es **desc**

*  **paramsSendOrderOrderAsc** -> Valor Ascendente para enviar a la peticion. Por defecto es **asc**

*  **paramsSendSearch** -> Si se incluye alguna busqueda en la tabla. Por defecto es **search**

*  **paramsSendPaginate** -> El numero de resultados por pagina que se deben mostrar. Por defecto es **paginate**

*  **paramsSendPage** -> La pagina en la que nos encontramos actualmente. Por defecto es **page**

*  **paramsSendColumns** -> Las columnas de la tabla incluidas en los parametros de inicializacion. Es un array con un objeto JSON dentro con los parametros especificados. Por defecto es **columns**


## Respuesta Dinamica

La respuesta se recogera en formato json (se esta trabajando para poder recibir en texto plano y xml). El formato de los campos a recoger son dinamicos pudiendo cambiarlos por parametros:

*  **paramsRequestState** -> Un String que se compara para comprobar si la llamada a sido satisfactoria o ha devuelto algun error. El campo con el que se compara se ve a continuacion. Por defecto es **state**

*  **paramsRequestStateOk** -> El String con el que es comparado el resultado de la peticion si es correcto. Por defecto es **OK**

*  **paramsRequestMsg*** -> Opcional. En caso de que la devolucion sea erronea diferente a **paramsRequestStateOk** se agregara un campo String con el mensaje a mostrar al usuario. Por defecto es **msg**

*  **paramsRequestData** -> Los datos de devolucion en caso que sea correcto. Se devolvera siempre en formato array. por defecto sera **data**. Cada objeto en formato json incluira como minimo estos campos:

	*  **paramsRequestLastPage** -> La ultima pagina de los resultados encontrados. Por defecto es **lastPage**

	*  **paramsRequestTotal** -> El total de datos existentes. Por defecto es **total**

*  **onCatch*** -> (Funcion)(Opcional) Si se recibe cualquier codigo 4xx o 5xx distinto de un 2xx o si ocurre algun error mientras se traspasa los datos a las variables ocurrira esto, y se ejecutara. La funcion contiene un parametro de entrada que es el error devuelto por axios.

El resto de datos se pintaran con el dbName que se les dio en columns para pintarlos exactamente en la columna correspondiente.

## Actualizaciones futuras


* Llamar al plugin sin vue teniendo este vue como motor independiente.

* Retirar vuesax de los estilos y coger estilos globales superiores para asi personalizarlo al tema.